.. _foreach_example: 


Data Palatalization with wfGenes 
====================================
    
However, Python makes it relatively easy to parallelize data and implement a "FOREACH" operation across multiple workers, wfGenes implements the FOREACH method to automate data parallelism. 



.. image:: /fig/map_reduce.png
  :alt: Description of the image
  :width: 400
  :align: center
  :height: 300


In order to automate the code generation, obviously, wfGenes requires additional information from the user. This additional information should be added to WConfig by providing elements to **func** list. The snippet code below presents WConfig structure of the YAML file for one node that generates necessary code to chunk the data into 10 parts and distribute them across 10 workers. 



.. code-block:: yaml

  workflow_name: simple FOREACH
  nodes:
  - name: plus_minus
    id: 1
    tasks:
    - func:
      - myfunctions
      - plus_minus
      - FOREACH
      - data
      - '10'
      inputs:
      - data
      - constant_y
      outputs:
      - datax
      - datay
      kwargs: {}




* **FOREACH**: This additional key directs wfGenes to generate necessary code to implement FOREACH data-flow on "data" as an input argument. The '10' is number of chunks of data or parallel processes.   


The snippets below are generated by wfGenes using the simple FOREACH WConfig, for Dask and Parsl.

1. Using Dask syntax during code generation phase. 

.. code-block:: Python

    lazy_plus_minus = []
    for i in range(0, len(data), int(len(data) / 10)):
        lazy_plus_minus_foreach = dask.delayed(nout=2)(plus_minus)(data[i:i + int(len(data) / 10)], constant_y)
        lazy_plus_minus.append(lazy_plus_minus_foreach)
    lazy_plus_minus = dask.compute(*lazy_plus_minus)
    lazy_plus_minus = flat_tuple(lazy_plus_minus, 2)


2. Using Parsl syntax during code generation phase. 

.. code-block:: Python

    parsl_plus_minus = []
    parsl_plus_minus_future = []
    parsl_data_generator = parsl_data_generator.result()
    for i in range(0, len(parsl_data_generator), int(len(parsl_data_generator) / 10)):
        parsl_plus_minus_foreach = plus_minus(
            parsl_data_generator[i:i + int(len(parsl_data_generator) / 10)], constant_y)
        parsl_plus_minus_future.append(parsl_plus_minus_foreach)
    for i in range(len(parsl_plus_minus_future)):
        parsl_plus_minus.append(parsl_plus_minus_future[i].result())
    parsl_plus_minus = flat_tuple(parsl_plus_minus, 2)


You can execute the generated code using wfGenes graphical executor. Additionally, the discussed WConfig  can be extended to include a second node and model a FOREACH task with two inputs. This node can chunk two inputs and distribute the resulting tasks among multiple workers.


.. code-block:: yaml

  - name: zipped FOREACH
    id: 2
    tasks:
    - func:
      - myfunctions
      - sum_square
      - FOREACH
      - datax
      - '10'
      - zip_inputs:
        - datay
      inputs:
      - datax
      - datay
      - constant_z
      outputs:
      - results
      kwargs: {}


* The **zip_inputs** enables users to add additional argument to FOREACH data flow and generate necessary code to perform parallel computation on two different inputs.

The snippets below are generated by wfGenes using the zipped FOREACH WConfig, for Dask and Parsl.


1. Using Dask syntax during code generation phase.

.. code-block:: Python

    lazy_sum_square = []
    for i in range(0, len(lazy_plus_minus[0]), int(len(lazy_plus_minus[0]) / 10)):
        lazy_sum_square_foreach = dask.delayed(nout=1)(sum_square)(
            lazy_plus_minus[0][i:i + int(len(lazy_plus_minus[0]) / 10)], lazy_plus_minus[1][i:i + int(len(lazy_plus_minus[0]) / 10)], constant_z)
        lazy_sum_square.append(lazy_sum_square_foreach)
    lazy_sum_square = dask.compute(*lazy_sum_square)
    lazy_sum_square = flat_list(lazy_sum_square)


2. Using Parsl syntax during code generation phase.

.. code-block:: Python

    parsl_sum_square = []
    parsl_sum_square_future = []
    for i in range(0, len(parsl_plus_minus[0]), int(len(parsl_plus_minus[0]) / 10)):
        parsl_sum_square_foreach = sum_square(parsl_plus_minus[0][i:i + int(len(parsl_plus_minus[0]) /10)], parsl_plus_minus[1][i:i +int(len(parsl_plus_minus[0]) /10)], constant_z)
        parsl_sum_square_future.append(parsl_sum_square_foreach)
    for i in range(len(parsl_sum_square_future)):
        parsl_sum_square.append(parsl_sum_square_future[i].result())
    parsl_sum_square = flat_list(parsl_sum_square)


The intro_examples folder of wfGenes's repository contains this particular example. The workflow depicted in the following graph shows nodes 1 and 2 running in parallel across multiple cores, with the average node computing the average of their output.

.. image:: /fig/foreach_graph.png
  :alt: Description of the image
  :width: 300
  :align: center
  :height: 300

The following plot is a simple performance analysis of generated code for the workflow above using wfGenes executor. Up to maximum number of n=32 parallel processes are tested. Since the experiment was carried out on a bwCloud instance equipped with 16 virtual CPUs, the scalability between 16 and 32 processes is comparable. The scalability is stronger for arrays length larger than 4000 elements since the multiprocessing overhead is less considerable. 

.. image:: /fig/parsl_foreach.png
  :alt: Description of the image
  :width: 600
  :align: center
  :height: 450



