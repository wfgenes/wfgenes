.. _handson:


Hands-on on wfGenes
=======================

This tutorial goes through three examples to examine wfGenes and supported WMSs using JupyterLab interface.

Setup 
=====================================

In order to complete the wfGenes setup on JupyterHub please complete `this guide <https://gitlab.com/wfgenes/wfgenes/-/blob/master/README_jupyterHUB.md>`_. 
Alternatively, you can have a look at `this guide <https://gitlab.com/wfgenes/wfgenes/-/blob/master/README.md>`_ and use pip python package manager to setup wfGenes on linux based operating systems. 



:ref:`Random DAG <rgg>` 
=====================================

Random task graph example demonstrates the capabilities of wfGenes in task coupling and parallelization by translating random graph (DAG) with hundreds of nodes to different workflow management systems.

:ref:`ForEach<foreach_example>`
=====================================

This example introduces convenient method to build workflows with data parallelization using different WMSs scaling from personal laptops to supercomputers with hundreds of cores.


:ref:`Adsorption Energy <multihith>`
=====================================

The adsorption energy example is considered as a part of this tutorial and as scientific application resembling RTG workflows.


:ref:`Motion Detection <motion_capture>`
=====================================
This example suggests wfGenes capability as a model-based design tool (MBD) to integrate different functions for application development. While each module is sequential and has single process, wfGenes generates parallel version of the model using Parsl or Dask libraries in an automated manner. 

:ref:`Hybrid Workflow  <hybrid_workflow>`
=====================================