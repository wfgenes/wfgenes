.. _multihith: 


Adsorption Energy
====================================

.. image:: /fig/multi_graph.png
  :alt: Description of the image
  :width: 600
  :align: center
  :height: 300

.. raw:: html

   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->

This tutorial demonstrates how wfGenes can be helpful for Computing the adsorption free energy of molecular oxygen (O2) on metal slab surface. This case study is modeled in three different versions and executed with wfGenes. The case study was part of GRK2450 program and presents wfGens capabilities as an interoperable approach to transfer workflows in computational nanoscience between two general-purpose WMSs, FireWorks and SimStack.

.. raw:: html

   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->

.. image:: /fig/adsorption_o2_flow.png
  :alt: Description of the image
  :width: 600
  :align: center
  :height: 300

.. raw:: html

   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->

.. image:: /fig/adsorption_o2.png
  :alt: Description of the image
  :width: 400
  :align: center
  :height: 200

.. raw:: html

   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->

The video below demonstrates how wfGenes can be used to generated necessary code for SimStack. Later, SimStack client can be used to launch generated workflow on the supercomputers. 

.. raw:: html

   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->


.. raw:: html

    <div class="youtube-container">
        <iframe class="youtube-video" src="https://www.youtube.com/embed/SWZrc0nvs0Y?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>

.. raw:: html

    <style>
        .youtube-container {
            position: relative;
            width: 100%;
            padding-bottom: 56.25%;
            overflow: hidden;
        }

        .youtube-video {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
    </style>
