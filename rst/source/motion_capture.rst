.. _motion_capture: 


Motion Detection
====================================

This example highlights the capability of wfGenes as a model-based design tool (MBD) for integrating various modules in order to ease application development for scientific computing. Despite each module being sequential and having a single process, wfGenes can generate a parallel version of the model using the Parsl or Dask libraries in an automated fashion.

.. raw:: html

   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->



.. image:: /fig/motion_cube.png
  :alt: Description of the image
  :width: 450
  :align: center
  :height: 300

.. raw:: html

   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->


.. image:: /fig/motion_fig.png
  :alt: Description of the image
  :width: 450
  :align: center
  :height: 300

.. raw:: html

   <br>  <!-- This creates a horizontal space -->

In order to complete this example, You can refer to motion_detection example in the intro_example folder. Additionally, the following YAML file demonstrates how wfGenes can be used to generate a wrapper for two independent functions and couple them in an automated manner. The first function counts the number of cameras connected to your laptop, while the second function detects motion for each camera. Since these parallel OpenCV functions require independent memory access, the wfGenes executor should use a **local process**.

.. raw:: html

   <br>  <!-- This creates a horizontal space -->



.. code-block:: yaml

  workflow_name: motion_capture_workflow
  nodes:
  - name: count cameras
    id: 1
    tasks:
    - func:
      - motion_detector
      - count_cameras
      inputs: []
      outputs:
      - cameras_count
      - cameras_index
      kwargs: {}
  - name: capture motion
    id: 2
    tasks:
    - func:
      - motion_detector
      - motion_control
      - FOREACH
      - cameras_index
      - full
      inputs:
      - cameras_index
      kwargs: {}  
      outputs: []
  metadata: 
    name: motion control
    category: introduction example
    feature: opencv, python



Please note that second node uses **FOREACH** data flow over **camera_index** (number of connected cameras to your machine), and **full** keyword tells wfGenes to run one process for each connected camera. In fact using **full** keyword enables users to spawn new process during run time and modify dataflow dynamically by partitioning fully the **camera_index** array.