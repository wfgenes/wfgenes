.. _server: 


Hello World Example
====================

1. First of all, you need to login to the cloud instance using your username and password. Althogh, you can easily copy paste commands in your terminal and procceed with hands-on step by step, modify **USERNAME** keyword accordingly.


.. code-block:: bash
    
    ssh -X USERNAME@193.196.38.24

After successful login, please change your password by executing:

.. code-block:: bash
    
    passwd

2. In order to proceed with the rest of the tutorial, you should first download git repository by issuing:

.. code-block:: bash
    
    git clone https://git.scc.kit.edu/th7356/simstack_tutorial




3. In the first exercise, we try to setup hello world example with SimStack and check previously described steps. To run the SimStack IDE run: 

.. code-block:: bash
    
    simsatck


4. From the left side of SimStack environment drag and drop Hello_GRK WaNo to nanomatch pane. Next, save the WaNo using GUI or alternatively press **ctrl+S** enter your desired name and press ok. You should see your workflow name at the bottom left of simstack environment. 


5. Next step is to configure your server configuration to establish secure connection and start your simulation. Please fill each field as below and save the setting.


.. code-block:: bash
    
    
    Registry name: simstack-server

    Base URI: server: server

    Username : your_username

    Basepath: simstack_workspace

    Queuing system: slurm 

    Directory on Cluster: /shared/software/nanomatch


6. Now you can run your simulation using GUI or alternatively by **ctrl+R**, 
please check the job status as described before through **jobs and workflow monitoring pane**. You can download all generated files to your machine and check the content.


Exercise 
==========

After completing hello world example, it is necessary to modify the WaNo files to learn how you can generate custom WaNo for the simulation code. Please close simstack and open an editor by running:


.. code-block:: bash

    gedit 


Navigate to downloaded git directory and open the **Repository** folder, inside the **Hello_GRK** sub-folder open the **Hello_GRK.xml** file and modify it to new version as below.  


.. code-block :: html

    <WaNoTemplate>
    <WaNoRoot name="Hello_GRK">
    <!--In WaNo root section  1)Name and 2)GUI parameters of the WaNo are defined-->     
    <WaNoMultipleOf name="Hello_Program">
    <Element id="0">
        <WaNoDictBox name="Simulation Box">
        <WaNoInt name="ID">0</WaNoInt>
        <WaNoString name="Name">0</WaNoString>
        </WaNoDictBox>
    </Element>
    </WaNoMultipleOf>
    </WaNoRoot>
    <WaNoInputFiles>
    <WaNoInputFile logical_filename="run_python.sh">run_python.sh</WaNoInputFile>
    <WaNoInputFile logical_filename="Hello_GRK.py">Hello_GRK.py</WaNoInputFile>
    </WaNoInputFiles>
    <WaNoExecCommand> bash ./run_python.sh </WaNoExecCommand>
    <WaNoOutputFiles>
        <WaNoOutputFile logical_filename="output.txt">output.txt</WaNoOutputFile>
    </WaNoOutputFiles>
    </WaNoTemplate>

Next, it is necessary to modify the **run_python.sh** accordingly, please refer to script below as reference:



.. code-block:: bash

    #!/bin/bash -eu

    {% for element in wano["Hello_Program"]%}
    
    python Hello_GRK.py {{element["counter"]}}  
    {% endfor %} > output.txt

After modification of theses two files, the WaNo is able to interact with user through GUI. As illustrated in figure below, two parameters can be set and passed to remote server via WaNO, moreover, multiple jobs with different setting can be set in one interactive session. 
For execution and monitoring your job please act as instructed before.


.. image:: /fig/exercise1.png 
   :width: 800
   :height: 400 


You can continue with next :ref:`next example <bill>` or back to :ref:`home <home>` page.





