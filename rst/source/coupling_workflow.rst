.. _multihith: 


Adsorption Energy
====================================

This tutorial demonstrates how wfGenes can be helpful for Computing the adsorption free energy of molecular oxygen (O2) on metal slab surface. This case study is modeled in three different versions and executed with wfGenes. The case study was part of GRK2450 program and presents wfGens capabilities as an interoperable approach to transfer workflows in computational nanoscience between two general-purpose WMSs, FireWorks and SimStack.



.. image:: /fig/adsorption_o2_flow.png
  :alt: Description of the image
  :width: 600
  :align: center
  :height: 300


.. image:: /fig/adsorption_o2.png
  :alt: Description of the image
  :width: 400
  :align: center
  :height: 200


The demo below demonstrates how wfGenes can be used to generated necessary code for SimStack. Later, SimStack client can be used to launch generated workflow on supercomputers. 



{'_tasks.0.script': './script_gpu.sh '}