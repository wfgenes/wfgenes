.. _rgg: 


Random task graph
====================================

Random task graph example demonstrates the capabilities of wfGenes in task coupling and code generation for different workflow management systems. We will see how the GUI provides necessary functionalities to model your workflow and use various executors to perform computation, scaling from personal to supercomputer centers. For the sake of simplicity, as the first example, we consider a simple sleep function that resembles computation in real life applications.  
As it is explained in the setup section, wfGenes’s GUI can be run on top Jupyter notebook either on your personal computers or on JupyterHUB.

.. raw:: html

   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->



.. raw:: html

    <div class="youtube-container">
        <iframe class="youtube-video" src="https://www.youtube.com/embed/GDLySgAimxM?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>

.. raw:: html

    <style>
        .youtube-container {
            position: relative;
            width: 100%;
            padding-bottom: 56.25%;
            overflow: hidden;
        }

        .youtube-video {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
    </style>





.. raw:: html

   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->



Remote Engine
====================================

Alternatively, we can use wfGene remote executor and setup the same workflow using local machine and perform computation on remote machine. Overall, working on a local machine and submitting jobs on remote machines can provide a flexible and efficient way to perform computational tasks while removing the burden on local hardware resources. However, in order to work with remote machines using your local machine, you need to complete certain steps beforehand. These steps are as follows:


1. Generate ssh key with strong pass phrase 
2. Registering your ssh key on  `bwIDM/bwServces <https://wiki.bwhpc.de/e/Registration/SSH>`_ . 
3. Start an SSH agent and add one or more SSH keys to it.

.. code-block:: bash
    
    eval $(ssh-agent); ssh-add



.. note:: 

  Interactive SSH Keys are not valid all the time, but only for one hour after the last 2-factor login. They have to be "unlocked" by entering the OTP and service password. 


Once you have completed these steps, you will be able to log in to the cluster using a passwordless SSH key and proceed with the example demonstrated in the tutorial below.




.. raw:: html

   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->

.. raw:: html

    <div class="youtube-container">
        <iframe class="youtube-video" src="https://www.youtube.com/embed/mitY4xQGuFA?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>

.. raw:: html

    <style>
        .youtube-container {
            position: relative;
            width: 100%;
            padding-bottom: 56.25%;
            overflow: hidden;
        }

        .youtube-video {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
    </style>

        

.. raw:: html

   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->




The example of a random task graph suggests how wfGenes is capable of coupling tasks and parallelizing them by translating the graph, which consists of thousands of nodes, to various WMSs that run on thousands of cores. However, for simple benchmarking we also modeled random task graph with ten nodes presented in the tutorial videos.

.. raw:: html

   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->
   <br>  <!-- This creates a horizontal space -->


.. image:: /fig/rgg_performance.png
  :alt: Description of the image
  :width: 600
  :align: center
  :height: 300