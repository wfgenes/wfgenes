

<img src="wfgenes_logo.png" width="200">

### wfGenes:
wfGenes is a tool to generate various type of workflow management systems (WMSs) by parsing single workflow configuration file called WConfig. 
Naturally, workflows are defined in human readable format, JSON or YAML, with efficient and concise structure to generate different type of WMSs by performing dependency analysis and automatic code generation for specific WMS. 
This approach enables users to examine different type of WMS based on the application requirement and available computing environment. 

### Installing wfGenes on Local machine:

The recommended way to install wfGenes is to use pip package manager. 

    pip install wfgenes

To use the GUI run the following line in a Jupyter notebook cell: 

    from wfgenes import wfgenes_gui



Website (including documentation): https://wfgenes.gitlab.io/wfgenes/

Source: https://git.scc.kit.edu/th7356/wfgenes
