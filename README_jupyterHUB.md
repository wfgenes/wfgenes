## Login to JupyterHub:

In order to login to JupyterHub for using bwUniCluster you need to have access right to corresponding machine. For complete guide on how to use JupyterHub please refer to [[1]], here we just briefly go through those steps for the sake of simplicity.    

First you can login to BwUnicluster using JupyterHub portal through the link below.

[JupyterHub Login Page](https://uc2-jupyter.scc.kit.edu/)

If you do not have the access right go ahead and complete the instruction in [[2]]. Otherwise, you should be able to complete the steps shown below.  

![anmeldung_demo](/fig/Jupyter_Anmeldung.gif)

**Remember to select jupyter/tensor in resources window as JupyterLab-Basemodule.**

### Adding kernel to Jupiter:

There are two main approaches to add your kernel to JupyterHub as a runtime environment. In both cases you should first purge loaded/default modules and proceed with following instructions.

      module purge 

1. Using virtualenv package which is the default python tool to create isolated run-time environments:

      Create new virtual environment using venv : 
      
            python -m venv "demo_env"
      
      Activate your environment :
      
            source demo_env/bin/activate

2. Create Virtual Environment with Anaconda (recommended):

            conda create -n demo_env python=3.10

      Activate your environment :
      
            conda activate demo_env


The rest is the same in two methods and you should create a kernel for Jupyter by issuing the command below: 


       python -m ipykernel install --user --name demo_env --display-name "Python (demo_env)"

Finally, you can install wfGenes using pip:

      pip install wfgenes


To use wfGenes GUI go ahead and open a notebook from the left hand pane and File -> New -> Notebook. 

![anmeldung_demo](/fig/switch_kernel.gif)

You can import the wfGenes GUI simply by running the line below in a notebook cell, depicted above.

      from wfgenes import wfgenes_gui

**Remember to unload Jupyter module from left side pane.**
       

### Remove Jupiter kernel:


When you finished with your experiment, after you deleted your virtual environment simply by deleting environment directory, you want to remove it also from Jupyter. Let’s first see which kernels are available [4]. You can list them with:

      jupyter kernelspec list

This should return something like:

Available kernels:

      demo_env   /home/user/.local/share/jupyter/kernels/demo_env
      python3    /usr/local/share/jupyter/kernels/python3

Now, to uninstall the kernel, you can type:

      jupyter kernelspec uninstall demo_env



[1]:https://wiki.bwhpc.de/e/BwUniCluster2.0/Jupyter
[2]:https://bwidm.scc.kit.edu/
