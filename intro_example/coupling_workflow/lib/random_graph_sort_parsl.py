import yaml
import numpy as np
import parsl
from parsl import python_app
from parsl.addresses import address_by_hostname
from parsl.providers import LocalProvider
from parsl.channels import LocalChannel
from parsl.config import Config
from parsl.executors import HighThroughputExecutor
from parsl.executors import ThreadPoolExecutor
from parsl.monitoring.monitoring import MonitoringHub
from parsl.providers import SlurmProvider
from parsl.launchers import SrunLauncher
from parsl.data_provider.files import File
from parsl.data_provider.file_noop import NoOpFileStaging
import time

from sort import sort_and_combine_arrays as __sort_and_combine_arrays
sort_and_combine_arrays = python_app(__sort_and_combine_arrays)


def main_parsl(array_y , **resource):

    start_time = time.time()
    if resource['pool'] == 'slurm':
        config = Config(
            executors=[
                HighThroughputExecutor(
                    label="frontera_htex",
                    address=address_by_hostname(),
                    max_workers=int(resource['worker_per_node']) * int(resource['scale']),
                    cores_per_worker=int(resource['cpu_per_node']) / int(resource['worker_per_node']),
                    provider=SlurmProvider(
                        channel=LocalChannel(),
                        cores_per_node=int(resource['cpu_per_node']),
                        nodes_per_block=int(resource['scale']),
                        mem_per_node=int(int(resource['memory']) / int(resource['scale'])),
                        init_blocks=1,
                        partition=str(resource['partition']),
                        walltime=str(resource['walltime']),
                        launcher=SrunLauncher(),
                    ),
                )
            ],
            strategy=None
        )

    elif resource['pool'] == "local_threads":
        config = Config(
            executors=[
                ThreadPoolExecutor(
                    label="frontera_htex",
                    max_threads=int(resource['threads_number']),
                )
            ],
            strategy=None
        )
    elif resource['pool'] == "local_processes":
        config = Config(
            executors=[
                HighThroughputExecutor(
                    label="frontera_htex",
                    address=address_by_hostname(),
                    max_workers=int(resource['processes_number']),
                    cores_per_worker=int(resource['cpu_per_process']),
                    provider=LocalProvider()
                )
            ],
            strategy=None
        )

    parsl.clear()
    parsl.load(config)
    # Read Input #1from subroutine #1 in routine #0


    end_memtime = time.time()

    # Start Step #1
    # Call subroutine #1 from routine #0

    parsl_sort_and_combine_arrays = sort_and_combine_arrays(array_y)
    # Call subroutine #1 from routine #1

    parsl_sort_and_combine_arrays_id1 = sort_and_combine_arrays(array_y)
    # Call subroutine #1 from routine #3

    parsl_sort_and_combine_arrays_id3 = sort_and_combine_arrays(array_y)
    # Call subroutine #1 from routine #6

    parsl_sort_and_combine_arrays_id6 = sort_and_combine_arrays(array_y)
    # End of step 1 with the width 4

    # Start Step #2
    # Call subroutine #1 from routine #2

    parsl_sort_and_combine_arrays_id2 = sort_and_combine_arrays(
        parsl_sort_and_combine_arrays_id1, parsl_sort_and_combine_arrays_id6)
    # Call subroutine #1 from routine #5

    parsl_sort_and_combine_arrays_id5 = sort_and_combine_arrays(parsl_sort_and_combine_arrays_id3)
    # Call subroutine #1 from routine #9

    parsl_sort_and_combine_arrays_id9 = sort_and_combine_arrays(parsl_sort_and_combine_arrays)
    # End of step 2 with the width 3

    # Start Step #3
    # Call subroutine #1 from routine #4

    parsl_sort_and_combine_arrays_id4 = sort_and_combine_arrays(
        parsl_sort_and_combine_arrays, parsl_sort_and_combine_arrays_id9)
    # Call subroutine #1 from routine #7

    parsl_sort_and_combine_arrays_id7 = sort_and_combine_arrays(parsl_sort_and_combine_arrays_id5)
    # Call subroutine #1 from routine #8

    parsl_sort_and_combine_arrays_id8 = sort_and_combine_arrays(parsl_sort_and_combine_arrays_id2)
    # End of step 3 with the width 3

    # Start Step #4
    # Call subroutine #1 from routine #10

    parsl_sort_and_combine_arrays_id10 = sort_and_combine_arrays(
        parsl_sort_and_combine_arrays_id4,
        parsl_sort_and_combine_arrays_id7,
        parsl_sort_and_combine_arrays_id8)
    # End of step 4 with the width 1
    print(parsl_sort_and_combine_arrays_id10.result())
    end_time = time.time()
    total_time = end_time - start_time
    total_memtime = end_memtime - start_time
    print(' The total time is', round(total_time, 2), 'seconds')
    print(' The total memory load time is', round(total_memtime, 2), 'seconds')
    print('PARSL based workflow')
    # The model has 4 step with the width of 4, 3, 3, 1,



if __name__ == "__main__" :

    resource = {
            "pool": "local_threads",
            "threads_number" : "4"     
    }
    
    array_y = [3, 5, 1, 4, 2]
    
    main (array_y, **resource)