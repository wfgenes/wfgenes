def sort_and_combine_arrays(*args):
    # Combine the input arrays into a single array
    combined_array = []
    for arr in args:
        combined_array.extend(arr)
    
    # Sort the combined array
    combined_array.sort()
    
    # Return the sorted, combined array
    return combined_array
