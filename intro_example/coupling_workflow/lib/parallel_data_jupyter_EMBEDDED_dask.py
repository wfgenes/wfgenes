import yaml
import numpy as np
import argparse
import dask
from dask.distributed import Client
from distributed.client import *
from dask_jobqueue import SLURMCluster
from BUILTIN import flat_tuple
from BUILTIN import flat_list
from BUILTIN import MERGE
from BUILTIN import wfgenes_argparser
import time


from myfunctions import plus_minus
from myfunctions import sum_square


def main_dask(data, constant_y, constant_z,  **resource):

    start_time = time.time()
    if resource['pool'] == 'slurm':
        cluster = SLURMCluster(
            cores=int(
                resource['cpu_per_node']),
            memory=str(
                resource['memory']) + 'GB',
            walltime=str(
                resource['walltime']),
            queue=str(
                resource['partition']),
            processes=int(
                resource['worker_per_node']))
        cluster.scale(int(resource['scale']))
        client = Client(cluster)
        print(cluster.job_script())
    elif resource['pool'] == 'local_threads':
        dask.config.set(scheduler='threads')
        dask.config.set(pool=ThreadPoolExecutor(int(resource['threads_number'])))
    elif resource['pool'] == 'local_processes':
        dask.config.set(scheduler='processes')
        dask.config.set(num_workers=int(resource['processes_number']))

    # Read Input #1 from subroutine #1 in routine #0

    end_memtime = time.time()

    # Start Step #1
    # Call subroutine #1 from routine #0

    lazy_plus_minus = []
    for i in range(0, len(data), int(len(data) / 10)):
        lazy_plus_minus_foreach = dask.delayed(nout=2)(plus_minus)(data[i:i + int(len(data) / 10)], constant_y)
        lazy_plus_minus.append(lazy_plus_minus_foreach)
    lazy_plus_minus = dask.compute(*lazy_plus_minus)
    lazy_plus_minus = flat_tuple(lazy_plus_minus, 2)
    # End of step 1 with the width 1

    # Start Step #2
    # Call subroutine #1 from routine #1

    lazy_sum_square = []
    for i in range(0, len(lazy_plus_minus[0]), int(len(lazy_plus_minus[0]) / 10)):
        lazy_sum_square_foreach = dask.delayed(nout=1)(sum_square)(
            lazy_plus_minus[0][i:i + int(len(lazy_plus_minus[0]) / 10)], lazy_plus_minus[1][i:i + int(len(lazy_plus_minus[0]) / 10)], constant_z)
        lazy_sum_square.append(lazy_sum_square_foreach)
    lazy_sum_square = dask.compute(*lazy_sum_square)
    lazy_sum_square = flat_list(lazy_sum_square)
    
    print(lazy_sum_square)
    # End of step 2 with the width 1
    # The model has 2 step with the width of 1, 1,
