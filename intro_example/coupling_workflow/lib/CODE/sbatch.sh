#!/bin/bash
#SBATCH -J mpi_job               # Job name
#SBATCH -o mpi_job.%j.out        # Output file name (%j expands to job ID)
#SBATCH -e mpi_job.%j.err        # Error file name (%j expands to job ID)
#SBATCH --nodes=16                # Number of nodes
#SBATCH --ntasks-per-node=4      # Number of tasks per node
#SBATCH --time=00:30:00          # Wall time limit (hh:mm:ss)
#SBATCH -p accelerated     # Partition name
#SBATCH --gres=gpu:4
# Load required modules
module load mpi/impi/2021.5.1

# Print the nodes allocated to the job
srun hostname

# Run MPI job
mpirun -np 64 ./main
