import ctypes
import numpy as np
import random
import time
import dask
import parsl
from parsl import python_app
from dask.distributed import Client
from parsl.executors import HighThroughputExecutor
from parsl.executors import ThreadPoolExecutor
from parsl.addresses import address_by_hostname
from parsl.channels import LocalChannel
from parsl.providers import SlurmProvider
from parsl.providers import LocalProvider
from parsl.launchers import SrunLauncher
from parsl.config import Config
import auxiliary
#import multiprocessing as mp

# Define the delayed functions










if __name__ == "__main__":
    
    resource = {}
    resource['pool'] ='slurm'

    if  resource['pool'] == 'slurm':  
        config = Config(
        executors=[
        HighThroughputExecutor(
                label="frontera_htex",
                address=address_by_hostname(),
                max_workers = 4,
                cores_per_worker = 1, 
                provider=SlurmProvider(
                    channel=LocalChannel(),
                    cores_per_node = 4,          
                    nodes_per_block=1,
                    mem_per_node = 100,
                    init_blocks=1,
                    partition='accelerated',
                    scheduler_options='#SBATCH --gres=gpu:4',
                    walltime = str("00:30:00"),
                    launcher=SrunLauncher(),
                ),
            )
        ],
            strategy = None
        )
        
    elif resource['pool'] == "local_threads":
        config = Config(
        executors=[
            ThreadPoolExecutor(
                label = "frontera_htex",
                max_threads=4,  
        )
        ],
            strategy = None
        )
    elif resource['pool'] == "local_processes":
        config = Config(
        executors=[
            HighThroughputExecutor(
                label="frontera_htex",
                address=address_by_hostname(),
                max_workers=4,
                cores_per_worker=1,
                provider=LocalProvider()
            )
        ],
        strategy=None
        )
    
    parsl.clear()
    parsl.load(config)



    
    start_time = time.time()
    result_1 = auxiliary.call_opencl_kernel(1).result()
    result_2 = auxiliary.call_opencl_kernel(2).result()
    result_1 = auxiliary.call_opencl_kernel(3).result()
    result_1 = auxiliary.call_opencl_kernel(4).result()
    #result_3 = auxiliary.call_opencl_kernel(3)
    #result_4 = auxiliary.call_opencl_kernel(4)
    #result_5 = auxiliary.call_opencl_kernel(5)
    #result_6 = auxiliary.call_opencl_kernel(6)
    #result_7 = auxiliary.call_opencl_kernel(7)
    
    #print(result_1.result(), result_2.result())
    #result = dask.compute(result_1, result_2, result_3, result_4)
    end_time = time.time()
    total_time = end_time - start_time
    print("Total dask compute time is", total_time)
    