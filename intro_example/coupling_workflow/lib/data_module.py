import random
from random_graph_sort_parsl import main_parsl
from parallel_data_jupyter_EMBEDDED_dask import main_dask



def prepare_data(length_data=''):
    array1 = [random.randint(0, 100) for _ in range(length_data)]
    array2 = [random.randint(0, 100) for _ in range(length_data)]
    return array1, array2



if __name__ == "__main__":
    
    array1, array2 = prepare_data(100)
    print(array1)
    print(array2)
    
    resource = {
            "pool": "local_threads",
            "threads_number" : "4"     
    }
    
    main_parsl(array1,**resource )
    main_dask(array2, 1, 2,**resource)

