""" Unit tests for wfGenes validation """ 

__author__ = 'Mehdi Roozmeh'
__copyright__ = 'Copyright 2020, Karlsruhe Institute of Technology'
__email__ = 'mehdi.roozmeh@kit.edu'
__maintainer__ = 'Mehdi Roozmeh'

import os,sys
import unittest
import subprocess
from pathlib import Path
import filecmp
from filecmp import dircmp
import argparse
import yaml
import json


class VALidatewfGenes (unittest.TestCase):
    """validate wfGenes against golden results """
    
    arg_list = [
     ['workflow_version_one.yaml', 'firework_adsorption_slab_version_one'], 
     ['workflow_version_two.yaml', 'firework_adsorption_slab_version_two'],
     ['workflow_version_three.yaml', 'firework_adsorption_slab_version_three']
    ]
    
    all_extension = ["*.xml", '*.dot' , '*.pdf' , '*.py', '*.yaml']
    
    test_path=Path(os.path.join(os.path.dirname(os.path.abspath(__file__))),'')
    golden_wfgenes=str(os.path.join(os.path.dirname(os.path.abspath(__file__)),'wgenerator.py'))
    project_path = str(test_path.parent)
    input_path = os.path.join(test_path.parent, 'runtime_input', '' )
    wconfig_path = os.path.join(test_path.parent, 'workflow_configuration','' )
    testcase_output = os.path.join(test_path, 'testcase_output','')
    golden_results_path = os.path.join(str(test_path), 'golden_results' , '')
    wfgene_path = os.path.join(project_path,'wfGenes_exe' , 'wgenerator.py')

    if not os.path.exists(golden_results_path):
        os.makedirs(golden_results_path)
        
    if not os.path.exists(testcase_output):
        os.makedirs(testcase_output)
    
    workflow_name=[]
    for argument in arg_list:
        interface_dict_1 = open(wconfig_path+argument[0], 'r')
        interface_dict = yaml.load(interface_dict_1, Loader=yaml.Loader)
        workflow_name.append(interface_dict['workflow_name'])
                    


    
    
    def test_Arun_wfGenes(self):
        """  Check if wgGenes run successfully """

        for i in range(len(self.arg_list)):
            arg = " --workflowconfig "+ self.wconfig_path+self.arg_list[i][0]+ " --inputpath "+ self.input_path+self.arg_list[i][1]+" --outputpath " + self.testcase_output+ " --wms all"
            cmd = "python "+ self.wfgene_path+arg 
            process = subprocess.Popen(cmd, shell=True)

            if process.wait() != 0:
                self.fail('Validation error: ')

            arg = " --workflowconfig "+ self.wconfig_path+self.arg_list[i][0]+ " --inputpath "+ self.input_path+self.arg_list[i][1]+" --outputpath " + self.golden_results_path+ " --wms all"
            cmd = "python "+ self.golden_wfgenes+arg 
            process = subprocess.Popen(cmd, shell=True)

            if process.wait() != 0:
                self.fail('Validation error: ')


    def test_fireworks_files(self):
        """ Validate generated YAML files """
        for i in range(len(self.arg_list)):
            cmd = "diff -r "
            golden_result = os.path.join(self.golden_results_path,self.workflow_name[i],'firework')
            testcase_result = os.path.join(self.testcase_output,self.workflow_name[i],'firework')
            cmd += golden_result+' '+ testcase_result
            process = subprocess.Popen(cmd, shell=True)
            if process.wait() != 0:
                self.fail('Validation error: ')

    def test_simstack_files(self):
        """ Validate generated XML files """
        for i in range(len(self.arg_list)):
            cmd = "diff -r "
            golden_result = os.path.join(self.golden_results_path,self.workflow_name[i],'simstack')
            testcase_result = os.path.join(self.testcase_output,self.workflow_name[i],'simstack')
            cmd += golden_result+' '+ testcase_result
            process = subprocess.Popen(cmd, shell=True)
            if process.wait() != 0:
                self.fail('Validation error: ')

    def test_parsl_files(self):
        for i in range(len(self.arg_list)):
            cmd = "diff -r "
            golden_result = os.path.join(self.golden_results_path,self.workflow_name[i],'parsl')
            testcase_result = os.path.join(self.testcase_output,self.workflow_name[i],'parsl')
            cmd += golden_result+' '+ testcase_result
            process = subprocess.Popen(cmd, shell=True)
            if process.wait() != 0:
                self.fail('Validation error: ')

    def test_dot_files(self):
        for i in range(len(self.arg_list)):
            cmd = "diff -r "
            golden_result = os.path.join(self.golden_results_path,self.workflow_name[i],'DOT')
            testcase_result = os.path.join(self.testcase_output,self.workflow_name[i],'DOT')
            cmd += golden_result+' '+ testcase_result
            process = subprocess.Popen(cmd, shell=True)
            if process.wait() != 0:
                self.fail('Validation error: ')
     
             
if __name__ == '__main__':         
    unittest.main()
