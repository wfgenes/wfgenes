

<img src="fig/wfgenes_logo.png" width="200">

####
This is a documentation for package maintainer(s). 

### Preparing pip package:

Install a project in editable mode (i.e. setuptools "develop mode") from a local project path or a VCS url. In order to complete the first step and prepare local pip package, the setup.cfg file is needed [1].   

      pip install --use-feature=in-tree-build e .

or (recommended)

      python setup.py develop    
    

Te second command creates a link to local repository instead of installing it in site-packages directory and python will use the local repository, do not forget to uninstall any previously installed version of the package before installing it from local repository. 



Afterwards, steps in [2] should be completed to prepare and publish the package. 



### Sync two repository:

  1  git remote set-url origin git@git.scc.kit.edu:th7356/wfgenes.git

  2 git pull origin  master

  3 git checkout -b new_version

  4 git remote set-url origin git@gitlab.com:wfgenes/wfgenes.git

  5 git push origin new_version

The rest of merge procedure should be done from the GUI. Finally, you should delete the local branch by issuing:
  
  6 git checkout master 

  7 git pull origin master

  
  8 git branch --delete new_version

  

[1]: https://setuptools.pypa.io/en/latest/userguide/declarative_config.html
[2]: https://packaging.python.org/en/latest/tutorials/packaging-projects/

